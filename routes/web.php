<?php
	/*
	**	Authentication
	*/
	Auth::routes();

	/*
	** Get's
	*/
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/edit_user', 'UserController@edit')->name('edit_user');
	Route::get('/client_management', 'ClientController@management')->name('client_management');
	Route::get('/schedule_management', 'ScheduleController@management')->name('schedule_management');
	Route::get('/new_client', 'ClientController@new_client_view')->name('new_client');

	/*
	** Post's
	*/
	Route::post('/save_contact', 'ContactController@save')->name('save_contact');

	/*
	** Put's
	*/
	Route::put('/save_edit_contact', 'ContactController@edit_save')->name('save_edit_contact');
	Route::put('/save_user','UserController@save')->name('save_user');

	/*
	** Delete's
	*/
	Route::delete('/remove_contact', 'ContactController@remove_contact')->name('remove_contact');
