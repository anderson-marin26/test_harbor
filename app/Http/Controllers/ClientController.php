<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use Auth;

class ClientController extends Controller
{
	public function management()
	{
		$where = array(
			'id' 	=> Auth::user()->id,
			'status'=> 1
		);

		$clients = Client::with('emails')->with('phones')->with('addresses')->where($where)->get()->toArray();

		return view('client_management')->with('clients', $clients);
	}

	public function new_view()
	{
		return view('new_client_view');
	}
}