<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class UserController extends Controller
{
	public function edit()
	{
		/*
		** Building object of the use
		*/
		$user = array(
			'id'    => Auth::user()->id,
			'name' 	=> Auth::user()->name,
			'email' => Auth::user()->email,
		);

		return view('edit_user')->with('user', $user);
	}

	public function save()
	{
		$where = array(
			'id' => $_POST['id_user']
		);

		$update = array(
			'name'  	=> $_POST['name'],
			'email' 	=> $_POST['email'],
		);

		if(!empty($_POST['password']) && $_POST['password'] != 'untouched')
		{
			$update['password'] = bcrypt($_POST['password']);
		}

		$user = User::where($where)->update($update);

		return view('home');
	}
}