<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\Email;
use App\Models\Phone;
use Auth;

class ContactController extends Controller
{
	public function new()
	{
		return view('new_contact');
	}

	public function save()
	{
		$now = date('Y-m-d H:i:s');

		$new_contact 				= new Contact();
		$new_contact->id 			= Auth::user()->id;
		$new_contact->name 			= $_POST['name'];
		$new_contact->create_date 	= $now;
		$new_contact->update_date	= $now;
		$new_contact->status 		= 1;
		$new_contact->save();

		foreach ($_POST['email'] as $email)
		{
			$new_contact_email				= new Email();
			$new_contact_email->id_contact 	= $new_contact->id_contact;
			$new_contact_email->email 		= $email;
			$new_contact_email->create_date = $now;
			$new_contact_email->update_date = $now;
			$new_contact_email->status 		= 1;
			$new_contact_email->save();
		}

		foreach ($_POST['phone'] as $phone)
		{
			$new_contact_phone 				= new Phone();
			$new_contact_phone->id_contact 	= $new_contact->id_contact;
			$new_contact_phone->phone 		= $phone;
			$new_contact_phone->create_date = $now;
			$new_contact_phone->update_date = $now;
			$new_contact_phone->status 		= 1;
			$new_contact_phone->save();
		}

		return redirect()->route('home');
	}

	public function edit()
	{
		$where = array(
			'id_contact' => $_GET['id_contact'],
            'status'=> 1
        );

        $contact = Contact::with('emails')->with('phones')->where($where)->first()->toArray();

		return view('edit_contact')->with('contact', $contact);
	}

	public function edit_save()
	{
		$status = array();
		$now = date('Y-m-d H:i:s');

		$update_contact = array(
			'name' 			=> $_POST['name'],
			'update_date' 	=> $now
		);

		$where_contact = array(
			'id_contact' => $_POST['id_contact']
		);

		$contact = Contact::where($where_contact)->update($update_contact);

		foreach ($_POST['email'] as $email_key => $email)
		{
			$update_mail = array(
				'email' 		=> $email,
				'update_date' 	=> $now
			);

			$where_mail = array(
				'id_email' => $email_key
			);

			$mail = Email::where($where_mail)->update($update_mail);
		}

		foreach ($_POST['phone'] as $phone_key => $phone)
		{
			$update_phone = array(
				'phone' 		=> $phone,
				'update_date' 	=> $now
			);

			$where_phone = array(
				'id_phone' => $phone_key
			);

			$phone = Phone::where($where_phone)->update($update_phone);

		}

		return redirect()->route('home')->with('contact', $contact);
	}

	public function remove_contact()
	{
		$update_remove = array(
			'update_date' 	=> date('Y-m-d H:i:s'),
			'status' 		=> 0
		);

		$where_remove = array(
			'id_contact' => $_POST['id_contact']
		);

		Contact::where($where_remove)->update($update_remove);
		Phone::where($where_remove)->update($update_remove);
		Email::where($where_remove)->update($update_remove);

		return redirect()->route('home')->with('remove_success', true);
	}
}