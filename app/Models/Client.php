<?php
	
	namespace App\Models;
	use Illuminate\Database\Eloquent\Model;

	class Client extends Model
	{
		public $timestamps		= false;
		protected $primaryKey 	= 'id_client';
		protected $table		= 'client';

		public function emails()
	    {
	        return $this->hasMany('App\Models\Email','id_client');
	    }

	    public function phones()
	    {
	    	return $this->hasMany('App\Models\Phone', 'id_client');
	    }

	    public function addresses()
	    {
	    	return $this->hasMany('App\Models\Address', 'id_client');
	    }
	}

