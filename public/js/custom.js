/*
** Adding input
*/
$('#plus_mail').click(function()
{
	var clone = $('#email').clone();

	// cleaning..
	clone.val('');

	$('#mail').append('<br>').append(clone);
});

$('#plus_phone').click(function()
{
	var clone = $('#phone').clone();

	// cleaning..
	clone.val('');

	$('#phones').append('<br>').append(clone);
});

$('.remove').click(function()
{
	var el = $(this);

	$('.confirm-remove').slideDown('fast');

    $('#yes').click(function()
    {
        $('.confirm-remove').slideUp('fast');

        el.parent().find('.remove_contact_form').submit();
    });

    $('#no').click(function(event)
    {
    	$('.confirm-remove').slideUp('fast');
    });
});