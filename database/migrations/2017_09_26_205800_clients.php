<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        ** Creating main table
        */
        Schema::create('client', function (Blueprint $table) {
            $table->increments('id_client');
            $table->integer('id');
            $table->string('name');
            $table->dateTime('create_date');
            $table->dateTime('update_date');
            $table->unsignedTinyInteger('status');
        });

        /*
        ** Creating email of the client
        */
        Schema::create('email', function(Blueprint $table) {
            $table->increments('id_email');
            $table->integer('id');
            $table->string('email');
            $table->dateTime('create_date');
            $table->dateTime('update_date');
            $table->unsignedTinyInteger('status'); 
        });

        /*
        ** Creating phone table of the client
        */
        Schema::create('phone', function(Blueprint $table) {
            $table->increments('id_phone');
            $table->integer('id');
            $table->string('phone');
            $table->dateTime('create_date');
            $table->dateTime('update_date');
            $table->unsignedTinyInteger('status'); 
        });

        /*
        ** Creating address table of the client
        */
        Schema::create('address', function(Blueprint $table) {
            $table->increments('id_address');
            $table->integer('id');
            $table->string('address');
            $table->string('number');
            $table->string('complement');
            $table->string('neighborhood');
            $table->string('city');
            $table->string('state');
            $table->dateTime('create_date');
            $table->dateTime('update_date');
            $table->unsignedTinyInteger('status'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client');
        Schema::dropIfExists('email');
        Schema::dropIfExists('phone');
        Schema::dropIfExists('address');
    }
}
