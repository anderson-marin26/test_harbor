@extends('layouts.app')

@section('content')
	<div class="container">
		<form class="form-horizontal" method="POST" action="{{ route('save_user') }}">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="id_user" value="{{ $user['id'] }}" />

            <div class="form-group">
                <label for="name" class="col-md-4 control-label">
                	Nome
            	</label>
                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ $user['name'] }}" required autofocus>
                </div>
                <br>
                <br>
                <label for="email" class="col-md-4 control-label">
                	Endereço de e-mail
            	</label>
                <div id="mail" class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ $user['email'] }}" required>
                </div>
                <br>
                <br>
                <label for="email" class="col-md-4 control-label">
                    Senha
                </label>
                <div id="mail" class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password" value="untouched" required>
                </div>
                <div class="col-md-6 col-md-offset-4" style="text-align: center; margin-top: 20px">
                    <button type="submit" class="btn btn-primary">
                        Salvar contato
                    </button>
                </div>
            </div>
        </form>
	</div>
@endsection
