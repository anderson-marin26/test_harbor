@extends('layouts.app')

@section('content')
<div class="container">
    <div class="confirm-remove text-center" style="font-size: 25px">
        <input type="hidden" id="id_extension" />
        Tem certeza que deseja remover o contato?
        <br />
        <br />
        <input type="button" id="yes" class="btn btn-danger" value="Sim" />
        <input type="button" id="no" class="btn btn-secondary" value="Não" />
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-4">
                        <form action="{{ route('edit_user') }}" method="get">
                            <input type="submit" class="btn btn-primary" value="Editar Usuario" style="width: 100%"/>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <form action="{{ route('client_management') }}" method="get">
                            <input type="submit" class="btn btn-primary" value="Gerenciamento de Cliente" style="width: 100%"/>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <form action="{{ route('schedule_management') }}" method="get">
                            <input type="submit" class="btn btn-primary" value="Gerenciamento de Agenda" style="width: 100%"/>
                        </form>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <!-- Welcome content -->
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
