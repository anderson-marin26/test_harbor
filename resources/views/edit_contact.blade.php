@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Editar contato</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('save_edit_contact') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <input name="id_contact" type="hidden" value="{{ $contact['id_contact'] }}" />

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">
                            	Nome
                        	</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required autofocus value="{{ $contact['name'] }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">
                            	Endereço de e-mail
                        	</label>
                            <div id="mail" class="col-md-6">
                                @foreach($contact['emails'] as $email)
                                    <input id="email" type="email" class="form-control" name="email[{{ $email['id_email'] }}]" required value="{{ $email['email'] }}">
                                    <br>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">
                            	Telefone
                        	</label>
                            <div id="phones" class="col-md-6">
                                @foreach($contact['phones'] as $phone)
                                    <input id="phone" type="number" class="form-control" name="phone[{{ $phone['id_phone'] }}]" required value="{{ $phone['phone'] }}">
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Editar contato
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
