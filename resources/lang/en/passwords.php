<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A senha precisa ter pelo menos 6 caracteres.',
    'reset' => 'Nova senha salva com sucesso',
    'sent' => 'Enviamos um e-mail para recuperação de senha!',
    'token' => 'Token de recuperação de senha invalido.',
    'user' => "Usuario não encontrado para o e-mail informado.",

];
