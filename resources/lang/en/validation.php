<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'O :attribute precisa ser aceito.',
    'active_url'           => 'O :attribute não é uma URL valida.',
    'after'                => 'O :attribute precisa ser uma data depois de :date.',
    'after_or_equal'       => 'O :attribute precisa ser uma data depois on igual a :date.',
    'alpha'                => 'O :attribute só deve conter letras.',
    'alpha_dash'           => 'O :attribute só deve conter letras, numeros a traços.',
    'alpha_num'            => 'O :attribute só deve conter letras e numeros.',
    'array'                => 'O :attribute deve ser um array.',
    'before'               => 'O :attribute precisa ser uma data antes de :date.',
    'before_or_equal'      => 'O :attribute precisa ser uma data antes ou igual a :date.',
    'between'              => [
        'numeric' => 'O :attribute precisa estar entre :min e :max.',
        'file'    => 'O :attribute precisa estar entre :min e :max kilobytes.',
        'string'  => 'O :attribute precisa estar entre :min e :max characters.',
        'array'   => 'O :attribute precisa ter entre :min e :max itens.',
    ],
    'boolean'              => 'O :attribute precisa ser verdadeiro ou falso.',
    'confirmed'            => 'O :attribute confirmação não corresponde.',
    'date'                 => 'O :attribute não é uma data valida.',
    'date_format'          => 'O :attribute não corresponde ao formato :format.',
    'different'            => 'O :attribute e :other precisam ser diferentes.',
    'digits'               => 'O :attribute precisa ser :digits digitos.',
    'digits_between'       => 'O :attribute precisa estar entre :min e :max digitos.',
    'dimensions'           => 'O :attribute tem dimensões de imagem invalidos.',
    'distinct'             => 'O :attribute campo tem valores duplicados.',
    'email'                => 'O :attribute precisa ser uma e-mail valido.',
    'exists'               => 'O item selecionado :attribute é invalido.',
    'file'                 => 'O :attribute precisa ser um arquivo.',
    'filled'               => 'O :attribute campo precisa ter um valor.',
    'image'                => 'O :attribute precisa ser uma imagem.',
    'in'                   => 'O item selecionado :attribute é invalido.',
    'in_array'             => 'O :attribute campo não existe em :other.',
    'integer'              => 'O :attribute precisa ser um inteiro.',
    'ip'                   => 'O :attribute precisa ser um endereço de IP valido.',
    'ipv4'                 => 'O :attribute precisa ser um endereço IPV4 valido.',
    'ipv6'                 => 'O :attribute precisa ser um endereço IPV4 valido.',
    'json'                 => 'O :attribute precisa ser uma string JSON valida.',
    'max'                  => [
        'numeric' => 'O :attribute não pode ser maior que :max.',
        'file'    => 'O :attribute não pode ser maior que :max kilobytes.',
        'string'  => 'O :attribute não pode ser maior que :max caracteres.',
        'array'   => 'O :attribute não pode ter mais que :max itens.',
    ],
    'mimes'                => 'O :attribute precisa ser uma arquivo do tipo :values.',
    'mimetypes'            => 'O :attribute precisa ser uma arquivo do tipo :values.',
    'min'                  => [
        'numeric' => 'O :attribute precisa ter no minimo :min.',
        'file'    => 'O :attribute precisa ter no minimo :min kilobytes.',
        'string'  => 'O :attribute precisa ter no minimo :min caracteres.',
        'array'   => 'O :attribute precisa ter no minimo :min itens.',
    ],
    'not_in'               => 'O item selecionado :attribute é invalido.',
    'numeric'              => 'O :attribute precisa ser um numero.',
    'present'              => 'O :attribute precisa estar presente.',
    'regex'                => 'O :attribute formato é invalido.',
    'required'             => 'O :attribute campo obrigatorio.',
    'required_if'          => 'O :attribute é obrigatorio quando :other é :value.',
    'required_unless'      => 'O :attribute campo é obrigatorio a menos que :other esteja em :values.',
    'required_with'        => 'O :attribute campo é obrigatorio quando :values esta presente.',
    'required_with_all'    => 'O :attribute campo é obrigatorio quanto :values esta presente.',
    'required_without'     => 'O :attribute campo é obrigatorio quanto :values não esta presente.',
    'required_without_all' => 'O :attribute field é obrigatorio quando nenhum :values estão presentes.',
    'same'                 => 'O :attribute e :other não correspondem.',
    'size'                 => [
        'numeric' => 'O :attribute precisa ser :size.',
        'file'    => 'O :attribute precisa ser :size kilobytes.',
        'string'  => 'O :attribute precisa ser :size caracteres.',
        'array'   => 'O :attribute precisa conter :size itens.',
    ],
    'string'               => 'O :attribute precisa ser uma string.',
    'timezone'             => 'O :attribute precisa ser uma zona valida.',
    'unique'               => 'O :attribute já esta em uso.',
    'uploaded'             => 'O :attribute falha no upload.',
    'url'                  => 'O :attribute formato invalido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
